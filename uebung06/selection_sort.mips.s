.data
lcg_a: .word 1664525
lcg_c: .word 1013904223
s_input_count: .asciiz "Wie viele Listen sollen sortiert werden? (Zahl zwischen 3 und 42)"
s_input_length: .asciiz "Wie lang sollen die Listen sein, die sortiert werden? (Zahl zwischen 10 und 100)"
s_input_seed: .asciiz "Bitte einen Startwert für die Listengeneration eingeben."
s_lb: .asciiz "["
s_rb: .asciiz "]"
s_ls: .asciiz ", "
s_nl: .asciiz "\n"
.align 4
input_data: .space 42 * 100 * 4

.text
main:
    jal get_input       # $s0: list count, $s1: list length, $s2: prng seed
    move $a0, $s2
    mult $s0, $s1       # multiply count and length
    mflo $a1
    jal lcg             # generate sweet simple randomness

    li $s3, 0           # current list index
main_loop:
    la $a0, input_data  # calculate current list offset
    mult $s1, $s3       # list length * current list number
    mflo $t0
    sll $t0, $t0, 2
    add $a0, $a0, $t0
    add $t0, $s1, -1    # calculate address of last list element
    sll $t0, $t0, 2
    add $a1, $a0, $t0
    jal dump_list       # print input list
    jal sort_list
    jal dump_list       # print sorted list
    la $a0, s_nl        # add an extra newline
    li $v0, 4
    syscall
    add $s3, $s3, 1
    blt $s3, $s0 main_loop

    j exit

swap:
    # a0, a1: swap values at these addresses
    lw $t0, ($a0)
    lw $t1, ($a1)
    sw $t0, ($a1)
    sw $t1, ($a0)
    j $ra

get_max:
    # a0, a1: address of first an last list element
    # results: v0 address, v1 value max element
    add $sp, $sp, -4        # reserve stack space
    sw $a0, 4($sp)          # save first argument on stack
    add $v0, $a0, 0         # init address
    lw $v1, ($a0)           # init value
loop_gm:
    add $a0, $a0, 4         # next element
    bgt $a0, $a1, end_gm    # work is finished
    lw $t0, ($a0)           # load next value
    ble $t0, $v1 skip_gm    # new max?
    move $v0, $a0           # if yes, save new address
    move $v1, $t0           # ...and value
skip_gm:
    j loop_gm
end_gm:
    lw $a0, 4($sp)          # restore $a0
    add $sp, $sp, 4         # restore $sp
    j $ra

dump_list:
    # a0, a1: address of first an last list element
    add $sp, $sp, -4        # reserve stack space
    sw $a0, 4($sp)          # save first argument on stack
    move $t0, $a0
    la $a0, s_lb            # print left bracket
    li $v0, 4
    syscall
dl_loop:
    lw $a0, ($t0)
    li $v0, 1               # print current list value
    syscall
    la $a0, s_ls            # print list separator
    li $v0, 4
    syscall
    add $t0, $t0, 4
    ble $t0, $a1, dl_loop
    la $a0, s_rb            # print right bracket
    li $v0, 4
    syscall
    la $a0, s_nl            # print newline
    syscall
    lw $a0, 4($sp)          # restore $a0
    add $sp, $sp, 4         # restore $sp
    j $ra

sort_list:
    # a0, a1: address of first an last list element
    # this could be easily changed to a simple loop without recursion
    add $sp, $sp, -12
    sw $ra, 12($sp)         # save $ra, because recursion (still a bad idea)
    sw $a0, 8($sp)          # another possibility would be to store $a0 and $a1 in $sx registers
    sw $a1, 4($sp)
    jal get_max             # v0 address, v1 value
    move $a1, $v0
    jal swap
    lw $a1, 4($sp)          # reset $a1 to last list element
    add $a0, $a0, 4
    subu $t0, $a0, $a1
    bltzal $t0, sort_list
    lw $a1, 4($sp)
    lw $a0, 8($sp)
    lw $ra, 12($sp)
    add $sp, $sp, 12
    j $ra

exit:
    li $v0, 10              # escape this jungle!
    syscall

# bonus content
get_input:
    la $a0, s_input_count
    li $v0, 4
    syscall
    # ß*§%@! spim does not support macros :(
    la $a0, s_nl
    syscall
    li $v0, 5               # get list count
    syscall
    li $t0, 3
    bge $v0, $t0 skip_c1    # if count < 10
    li $v0, 3              # set count = 10
skip_c1:
    li $t0, 42
    ble $v0, $t0, skip_c2   # if count > 100
    li $v0, 42             # set count = 100
skip_c2:
    move $s0, $v0

    la $a0, s_input_length
    li $v0, 4
    syscall
    la $a0, s_nl
    syscall
    li $v0, 5               # get list length
    syscall
    li $t0, 10
    bge $v0, $t0 skip_l1    # if length < 10
    li $v0, 10              # set length = 10
skip_l1:
    li $t0, 100
    ble $v0, $t0, skip_l2   # if length > 100
    li $v0, 100             # set length = 100
skip_l2:
    move $s1, $v0

    la $a0, s_input_seed
    li $v0, 4
    syscall
    la $a0, s_nl
    syscall
    li $v0, 5               # get seed for prng
    syscall
    move $s2, $v0
    j $ra

lcg:
    # arguments: $a0: seed, $a1 count
    li $t0, 0
    la $t1, input_data
    lw $t2, lcg_a
    lw $t3, lcg_c
lcg_loop:                   # simple linear congruential generator
    mult $a0, $t2
    mflo $a0
    addu $a0, $a0, $t3
    srl $t4, $a0, 16        # only use upper 16 bit for now
    sw $t4, ($t1)           # store generated number to list
    add $t0, $t0, 1         # increment counter
    add $t1, $t1, 4         # increment address
    ble $t0, $a1, lcg_loop
    j $ra

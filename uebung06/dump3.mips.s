.data
test: .word 17, 34, -1, 12, 50, 20, 3, -1001, 1564, 27
test_end: .word 25
test2: .word 12, 7, 9, -4, 22, 256, 3520, -245, 55, 777, 99
test2_end: .word 101
test3: .word 9, 17, 8, 22, -5, 23, 42, 15, -5, -42, -99, 40
test3_end: .word 39
s_lb: .asciiz "["
s_rb: .asciiz "]"
s_ls: .asciiz ", "
s_nl: .asciiz "\n"

.text
main:
    la $a0, test
    la $a1, test_end
    jal dump_list
    jal sort_list
    jal dump_list
    la $a0, s_nl
    li $v0, 4
    syscall

    la $a0, test2
    la $a1, test2_end
    jal dump_list
    jal sort_list
    jal dump_list
    la $a0, s_nl
    li $v0, 4
    syscall

    la $a0, test3
    la $a1, test3_end
    jal dump_list
    jal sort_list
    jal dump_list

    j exit

swap:
    # a0, a1: swap values at these addresses
    lw $t0, ($a0)
    lw $t1, ($a1)
    sw $t0, ($a1)
    sw $t1, ($a0)
    j $ra

get_max:
    # a0, a1: address of first an last list element
    # results: v0 address, v1 value max element
    add $sp, $sp, -4        # reserve stack space
    sw $a0, 4($sp)          # save first argument on stack
    add $v0, $a0, 0         # init address
    lw $v1, ($a0)           # init value
loop_gm:
    add $a0, $a0, 4         # next element
    bgt $a0, $a1, end_gm    # work is finished
    lw $t0, ($a0)           # load next value
    ble $t0, $v1 skip_gm    # new max?
    move $v0, $a0           # if yes, save new address
    move $v1, $t0           # ...and value
skip_gm:
    j loop_gm
end_gm:
    lw $a0, 4($sp)          # restore $a0
    add $sp, $sp, 4         # restore $sp
    j $ra

dump_list:
    # a0, a1: address of first an last list element
    add $sp, $sp, -4        # reserve stack space
    sw $a0, 4($sp)          # save first argument on stack
    move $t0, $a0
    la $a0, s_lb            # print left bracket
    li $v0, 4
    syscall
dl_loop:
    lw $a0, ($t0)
    li $v0, 1               # print current list value
    syscall
    la $a0, s_ls            # print list separator
    li $v0, 4
    syscall
    add $t0, $t0, 4
    ble $t0, $a1, dl_loop
    la $a0, s_rb            # print right bracket
    li $v0, 4
    syscall
    la $a0, s_nl            # print newline
    syscall
    lw $a0, 4($sp)          # restore $a0
    add $sp, $sp, 4         # restore $sp
    j $ra

sort_list:
    # a0, a1: address of first an last list element
    # this could be easily changed to a simple loop without recursion
    add $sp, $sp, -12
    sw $ra, 12($sp)         # save $ra, because recursion (still a bad idea)
    sw $a0, 8($sp)
    sw $a1, 4($sp)
    jal get_max             # v0 address, v1 value
    move $a1, $v0
    jal swap
    lw $a1, 4($sp)
    add $a0, $a0, 4
    subu $t0, $a0, $a1
    bltzal $t0, sort_list
    lw $a1, 4($sp)
    lw $a0, 8($sp)
    lw $ra, 12($sp)
    add $sp, $sp, 12
    j $ra

exit:
    li $v0, 10              # escape this jungle!
    syscall

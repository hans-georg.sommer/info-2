.data
test: .word 17
test2: .word 2
.text
main:
    la $a0, test
    la $a1, test2
swap:
    # a0, a1: Die Werte an diesen Adressen werden vertauscht
    lw $t0, ($a0)
    lw $t1, ($a1)
    sw $t0, ($a1)
    sw $t1, ($a0)

exit:
    li $v0, 10
    syscall

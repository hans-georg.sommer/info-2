.data
test: .word 17, 34, -1, 12, 50, 20, 3, -1001, 1564, 25
test2: .word 1, 7, 9, -4, 22, 256, 3520, -245, 55, 777, 99, 101
test3: .word 9, 17, 8, 22, -5, 23, 42, 15, -5, -42, -99, 40, 39
test42:
nl: .asciiz "\n"

.text
main:
    la $a0, test
    la $a1, test2
    jal get_max
    jal output_test
    la $a0, test2
    la $a1, test3
    jal get_max
    jal output_test
    la $a0, test3
    la $a1, test42
    jal get_max
    jal output_test
    j exit

swap:
    # a0, a1: Die Werte an diesen Adressen werden vertauscht
    lw $t0, ($a0)
    lw $t1, ($a1)
    sw $t0, ($a1)
    sw $t1, ($a0)

get_max:
    # a0, a1: Adressen Erstes und letztes Element der Liste
    # Ergebnisse: v0 Adresse, v1 Wert größtes Element
    add $v0, $a0, 0         # Initialisierung Adresse
    lw $v1, ($a0)           # Initialisierung Wert
loop_gm:
    add $a0, $a0, 4         # nächstes Element
    bgt $a0, $a1, end_gm    # Liste ist bearbeitet
    lw $t0, ($a0)           # neuen Wert laden
    ble $t0, $v1 skip_gm    # neues Maximum?
    add $v0, $a0, 0         # wenn ja, neue Adresse...
    add $v1, $t0, 0         # ... und Wert übernehmen
skip_gm:
    j loop_gm
end_gm:
    j $ra

output_test:
    add $a0, $v0, 0
    li $v0, 1   # Adresse des Maximums ausgeben
    syscall
    la $a0, nl
    li $v0, 4   # newline ausgeben
    syscall
    add $a0, $v1, 0
    li $v0, 1   # Wert des Maximums ausgeben
    syscall
    la $a0, nl
    li $v0, 4   # newline ausgeben
    syscall
    j $ra

exit:
    li $v0, 10              # weg von hier
    syscall

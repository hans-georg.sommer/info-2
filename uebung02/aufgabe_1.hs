type Nibble = (Bool, Bool, Bool, Bool)

-- helper functions

-- convert a list of Boolean values to a binary string
bool2binString :: [Bool] -> String
bool2binString [] = ""
bool2binString bl
    | head bl = '1' : bool2binString (tail bl)
    | otherwise = '0' : bool2binString (tail bl)

-- convert a list of Boolean values to a decimal number
bool2dec :: [Bool] -> Int
bool2dec [] = 0
bool2dec bl
    | head bl = 2 ^ length(tail bl) + (bool2dec (tail bl))
    | otherwise = bool2dec (tail bl)

-- convert a list of Boolean values to a decimal number using two's complement
bool2decTwoComp :: [Bool] -> Int
bool2decTwoComp [] = 0
bool2decTwoComp bl
    | head bl = (-1) * (2 ^ length(tail bl)) + (bool2dec (tail bl))
    | otherwise = bool2dec bl

xor :: Bool -> Bool -> Bool
xor a b = (a || b) && (not (a && b))

------------
-- Aufgabe 1
------------

-- show the binary, decimal an decimal/two's complement representations of a nibble
showNibble :: Nibble -> String
showNibble x = bool2binString n ++ " " ++ show (bool2dec n) ++ " " ++ show (bool2decTwoComp n)
    where n = [a, b, c, d]
          (a, b, c, d) = x

------------
-- Aufgabe 2
------------

fulladder :: Bool -> Bool -> Bool -> (Bool, Bool)
fulladder a b c_in = (c_out, s)
    where c_out = (a && b) || (t && c_in)
          s = xor t c_in
          t = xor a b

------------
-- Aufgabe 3
------------

rippleCarryAdder :: Nibble -> Nibble -> Nibble
rippleCarryAdder a b = (s3, s2, s1, s0)
    where (a3, a2, a1, a0) = a -- this i really ugly, but i am to lazy,
          (b3, b2, b1, b0) = b -- to implement something better for nibble handling
          (c1, s0) = fulladder a0 b0 False
          (c2, s1) = fulladder a1 b1 c1
          (c3, s2) = fulladder a2 b2 c2
          (c_out, s3) = fulladder a3 b3 c3

------------
-- Aufgabe 4
------------

tableAdderLine :: (Nibble -> Nibble -> Nibble) -> Nibble -> Nibble -> String
tableAdderLine f a b = showNibble a ++ " ; " ++ showNibble b ++ " ; " ++ showNibble (f a b)

tableAdder :: (Nibble -> Nibble -> Nibble) -> [(Nibble, Nibble)] -> String
tableAdder f ntl
    | length ntl == 1 = tableAdderLine f a b
    | otherwise = tableAdderLine f a b ++ "\n" ++ tableAdder f (tail ntl)
    where a = fst (head ntl)
          b = snd (head ntl)

------------------------
-- Test ist all at once!
------------------------
testItAll :: String
testItAll = tableAdder rippleCarryAdder[((a, b, c, d), (e, f, g, h)) |
            a <- [False, True],
            b <- [False, True],
            c <- [False, True],
            d <- [False, True],
            e <- [False, True],
            f <- [False, True],
            g <- [False, True],
            h <- [False, True]]

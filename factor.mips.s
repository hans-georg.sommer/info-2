# Saaluebung 04.07.2017

main:
    li $a0, -7
    li $a1, 2
    jal power
    move $a0, $v0
    li $v0, 1
    syscall
    li $v0, 10
    syscall

power:
    add $sp, $sp, -4
    sw $ra, 4($sp)
    add $a1, $a1, -1
    ble $a1, $zero, loop
    li $v0, 1
    j power_end
loop:
    jal power
power_end:
    mulo $v0, $v0, $a0
    lw $ra, 4($sp)
    add $sp, $sp, 4
    j $ra

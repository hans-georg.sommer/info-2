quadratic :: (Int , Int , Int) -> Int -> Int
quadratic (a, b, c) x = a * x*x + b * x + c



square :: Int -> Int
square n
    | n < 0     = square (-n)
    | n == 0    = 0
    | otherwise = 2*n - 1 + square (n-1)



sumList :: [Int] -> Int
sumList [] = 0
sumList is = head is + sumList (tail is)



foldList :: (Double -> Double -> Double) -> [Double] -> Double
foldList f ds
    | length ds == 1 = head ds
    | otherwise      = f (head ds) (foldList f (tail ds))



mapList :: (Int -> Int) -> [Int] -> [Int]
mapList f is
    | is == []  = []
    | otherwise = [f (head is)] ++ (mapList f (tail is))



tableInt :: (Int -> Int) -> [Int] -> String
tableInt f is
    | length is == 1 = show (head is) ++ " : " ++ show (f (head is)) --ugly, but no newline at the end
    | otherwise      = show (head is) ++ " : " ++ show (f (head is)) ++ "\n" ++ (tableInt f (tail is))

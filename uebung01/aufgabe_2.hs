import Data.Char (toLower)
--note: ghci cannot parse import statements later -.-

containsList :: [Int] -> Int -> Bool
containsList xs x
    | xs == []     = False
    | head xs == x = True
    | otherwise    = containsList (tail xs) x



countList :: [Char] -> Char -> Int
countList [] c = 0
countList cs c
    | toLower (head cs) == lc = 1 + countList (tail cs) lc
    | otherwise = 0 + countList (tail cs) lc
    where lc = toLower c



bubbleSort :: [Int] -> [Int]
bubbleSort xs
    | length xs == 1    = xs
    | head xs < xs !! 1 = [xs !! 1] ++ bubbleSort ([head xs] ++ tail (tail xs))
    | otherwise         = [head xs] ++ bubbleSort (tail xs)



bubbleSortList :: [Int] -> [Int]
bubbleSortList [] = []
bubbleSortList xs
    | length xs == 1  = xs
    | xs == xs_sorted = xs
    | otherwise       = bubbleSortList xs_sorted
    where xs_sorted   = bubbleSort xs

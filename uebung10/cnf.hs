-- Vorgaben

import Data.List

data Literal
    = Atom {atomChar :: Char}
    | NegA {atomChar :: Char}
    deriving Eq

instance Show Literal where
    show (Atom a) = [a]
    show (NegA a) = ['~', a]

data Clause = Clause [Literal]
data CNF = CNF [Clause]

------------
-- Aufgabe 1
------------

--printOpList :: Show a => [a] -> String -> String
--printOpList [] s = ""
--printOpList ls s = s ++ show (head ls) ++ (printOpList (tail ls) s)

--instance Show Clause where
--    show (Clause c) = "(" ++ show (head c) ++ (printOpList (tail c) " v ") ++ ")"

instance Show Clause where
    show (Clause c) = "(" ++ show (intercalate " v " [map show c]) ++ ")"

--instance Show CNF where
--    show (CNF c) = show (head c) ++ (printOpList (tail c) " ^ ")

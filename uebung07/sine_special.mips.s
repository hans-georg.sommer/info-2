.data

d_zero: .double 0.0
d_one:  .double 1.0
d_two:  .double 2.0

str_input:  .asciiz "Bitte eine Zahl x eingeben.\n"
str_input2: .asciiz "Bitte die gewünschte Genauigkeit eingeben.\n"
str_again:  .asciiz "Das war lustig. Möchtest du nochmal? ('n' für nö, alles andere heißt ja.)"
str_nl:     .asciiz "\n"

.text

factorial:
    # input in $f0, $f1
    # output in $f12, $f13
    add $sp, $sp, -8
    s.d $f2, 4($sp)     # save $f2 on stack
    abs.d $f0, $f0      # no negative values allowed
    l.d $f2, d_zero
    c.le.d $f0, $f2     # 0! == 1
    bc1t set_one
    l.d $f2, d_one      # load 1.0 for comparison and substraction
    mov.d $f12, $f0     # calc factorial in $f12
factorial_loop:         # simple loop
    sub.d $f0, $f0, $f2
    c.le.d $f0, $f2
    bc1t factorial_end
    mul.d $f12, $f12, $f0
    j factorial_loop
set_one:
    l.d $f12, d_one
factorial_end:
    l.d $f2, 4($sp)     # restore $f2 from stack
    add $sp, $sp, 8
    jr $ra

sine:
    # input: $f0, $f1 - x, $f2, $f3 - precision epsilon
    # output in $12, $13
    add $sp, $sp, -12
    sw $ra, 4($sp)          # save return address, because we go one step further
    li $s0, 1               # one always need a simple 1 ;)
    mov.d $f6, $f0          # $f6: current polynomial x ^ 2i-1
    l.d $f8, d_one          # $f8: 2i-1 for factorial calculation
    mul.d $f14, $f0, $f0    # $f14: const x ^ 2
    l.d $f20, d_two         # $f20: const 2.0
sine_loop:
    mul.d $f6, $f6, $f14    # new polynomial
    add.d $f8, $f8, $f20    # new 2i-1
    s.d $f0, 8($sp)
    mov.d $f0, $f8          # move to sub input reg
    jal factorial           # call factorial, result in $f12
    l.d $f0, 8($sp)
    div.d $f4, $f6, $f12
    c.lt.d $f4, $f2         # compare to epsilon
    bc1t sine_end
special:                    # everybody likes specials
    sub.d $f0, $f0, $f4     # accumulate result
    lw $t0, special
    xor $t0, $t0, $s0
    sw $t0, special
    j sine_loop
sine_end:
    mov.d $f12, $f0         # move result to output reg
    lw $ra, 4($sp)          # restore return address
    add $sp, $sp, 12
    jr $ra

main:
    la $a0, str_input
    li $v0, 4           # print string
    syscall
    li $v0, 7           # read double x
    syscall
    add $sp, $sp, -8
    s.d $f0, 4($sp)
    la $a0, str_input2
    li $v0, 4
    syscall
    li $v0, 7           # read double epsilon
    syscall
    mov.d $f2, $f0      # move epsilon to $f2
    l.d $f0, 4($sp)
    add $sp, $sp, 8
    jal sine
    li $v0, 3           # print result
    syscall
    la $a0, str_nl
    li, $v0, 4
    syscall
    syscall             # two newlines are better than one
    jal doyouwantanotherone
    j main

exit:
    li $v0, 10          #exit
    syscall

doyouwantanotherone:
    la $a0, str_again
    li $v0, 4
    syscall
    add $sp $sp, -4
    add $a0, $sp, 4
    li $a1, 2
    li $v0, 8           # read string
    syscall
    lw $t0, 4($sp)
    sll $t0, $t0, 0x18
    srl $t0, $t0, 0x18
    li $t1, 0x6e
    beq $t0, $t1, exit
    la $a0, str_nl
    li, $v0, 4
    syscall
    add $sp, $sp, 4
    jr $ra

.data

d_zero: .double 0.0
d_one:  .double 1.0

str_input: .asciiz "Bitte eine Zahl eingeben.\n"

.text

factorial:
    # input in $f0, $f1
    # output in $f12, $f13
    add $sp, $sp, -8
    s.d $f2, 8($sp)     # used nowhere, save anyway
    abs.d $f0, $f0      # no negative values allowed
    floor.w.d $f0, $f0  # calc only int factorial, still with precision lost
    l.d $f2, d_zero
    c.eq.d $f0, $f2     # 0! == 1
    bc1t set_one
    l.d $f2, d_one      # load 1.0 for comparison and substraction
    mov.d $f12, $f0     # calc factorial in $f12
factorial_loop:         # simple loop
    sub.d $f0, $f0, $f2
    c.le.d $f0, $f2
    bc1t factorial_end
    mul.d $f12, $f12, $f0
    j factorial_loop
set_one:
    l.d $f12, d_one
factorial_end:
    l.d $f2, 8($sp)     # restore $f2 from stack
    add $sp, $sp, 8
    j $ra

sine:
    # input: $f0, $f1 - x, $f2, $f3 - precision epsilon
    # output in $12, $13

main:
    la $a0, str_input
    li $v0, 4           # print string
    syscall
    li $v0, 7           # read double
    syscall
    jal factorial
    li $v0, 3
    syscall
    j exit

exit:
    li $v0, 10          #exit
    syscall

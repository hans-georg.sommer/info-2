import Data.Maybe

------------
-- Aufgabe 2
------------

match :: Char -> Maybe String -> Maybe String
match c s
    | isNothing s = Nothing
    | c == (head (fromJust s)) = Just (tail (fromJust s))
    | otherwise = Nothing

term :: Maybe String -> Maybe String
term s
    | (isNothing s) = Nothing
    | otherwise = ftail (factor s)

ttail :: Maybe String -> Maybe String
ttail s
    | (isNothing s) = Nothing
    | not (isNothing res) = res
    | otherwise = s
    where res = ttail (term (match '+' s))

factor :: Maybe String -> Maybe String
factor s
    | (isNothing s) = Nothing
    | otherwise = match 'c' s

ftail :: Maybe String -> Maybe String
ftail s
    | (isNothing s) = Nothing
    | not (isNothing res) = res
    | otherwise = s
    where res = ftail (factor (match '*' s))

expr :: Maybe String -> Maybe String
expr s
    | (isNothing s) = Nothing
    | otherwise = ttail ( term s )

prog :: String -> Maybe String
prog s = match '$' (expr (Just s))

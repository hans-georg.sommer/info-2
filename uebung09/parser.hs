import Data.Maybe

------------
-- Aufgabe 1
------------

errormsg :: a
errormsg = error "Das ist nicht akzeptabel!"

match :: String -> [String] -> [String]
match s [] = errormsg
match s l
    | s == head l = tail l
    | otherwise   = errormsg

id_list_end :: [String] -> [String]
id_list_end [] = []
id_list_end l  = id_list_end (match "$$" (match ";" l))

id_list_tail :: [String] -> [String]
id_list_tail l
    | l == [] = errormsg
    | head l == ";" = id_list_end l
    | otherwise     = id_list_tail (match "id" (match "," l))

id_list :: [String] -> [String]
id_list l
    | tail l == [] = errormsg
    | otherwise    = id_list_tail (match "id" l)

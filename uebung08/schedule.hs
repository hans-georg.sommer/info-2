import Data.List

data Prozess = Prozess {
    pid :: String,
    arrival :: Int,
    computing :: Int
    } deriving Show

instance Eq Prozess where
    x == y = computing x == computing y

instance Ord Prozess where
    compare x y = compare (computing x) (computing y)

data State = State {
    new   :: [Prozess],
    run   :: Prozess,
    ready :: [Prozess],
    time  :: Int,
    chart :: String
    }

------------
-- Aufgabe 1
------------

instance Show State where
    show s = "-- new\n" ++ unlines (map show (new s)) ++
             "-- run\n" ++ show (run s) ++ "\n" ++
             "-- ready\n" ++ unlines (map show (ready s)) ++
             "-- time: " ++ show (time s) ++ "\n" ++
             "-- chart: " ++ (chart s)

------------
-- Aufgabe 2
------------

splitProzessListByTime :: Int -> [Prozess] -> ([Prozess], [Prozess])
splitProzessListByTime t [] = ([], [])
splitProzessListByTime t (p : pl)
    | (arrival p) > t = (p : l, r)
    | otherwise       = (l, p : r)
    where (l, r) = splitProzessListByTime t pl

update_ready :: State -> State
update_ready s = s {new = n, ready = (ready s) ++ r}
    where (n, r) = splitProzessListByTime (time s) (new s)

------------
-- Aufgabe 3
------------

update_run :: State -> State
update_run s
    | pid (run s) == "IDLE" && (ready s) /= [] = s {run = head (ready s), ready = tail (ready s)}
    | otherwise = s

------------
-- Aufgabe 4
------------

update_time :: State -> State
update_time s
    | computing (run s) > 0 = s {run = (run s) {computing = computing (run s) - 1}, time = nt, chart = (chart s) ++ pid (run s) ++ ", "}
    | computing (run s) == 0 && (ready s) /= [] = s {run = next_p {computing = computing next_p - 1}, ready = tail (ready s), time = nt, chart = (chart s) ++ pid (head (ready s)) ++ ", "}
    | otherwise = s {run = idle, time = nt, chart = (chart s) ++ "IDLE, "}
    where nt = (time s) + 1
          next_p = head (ready s)
          idle = Prozess {pid = "IDLE", arrival = -1, computing = -1}

------------
-- Aufgabe 5
------------

update_fcfs :: State -> State
update_fcfs s = update_time (update_run (update_ready s))

------------
-- Aufgabe 6
------------

-- Shortest job next
update_ready_sjn :: State -> State
update_ready_sjn s = s {new = n, ready = sort ((ready s) ++ r)}
    where (n, r) = splitProzessListByTime (time s) (new s)

update_sjn :: State -> State
update_sjn s = update_time (update_run (update_ready_sjn s))

-- Shortest job first
update_run_sjf :: State -> State
update_run_sjf s
    | pid (run s) == "IDLE" && (ready s) /= [] = s {run = head (ready s), ready = tail (ready s)}
    | otherwise = s {run = r, ready = rem}
    where (r : rem) = sort (run s: ready s)

update_sjf :: State -> State
update_sjf s = update_time (update_run_sjf (update_ready_sjn s))

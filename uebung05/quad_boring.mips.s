.data
n:  .asciiz "\n"

.text
main:
    li $a0, 0 # Summe
    li $t0, 1 # Summand
    li $t1, 0 # counter
    li $v0, 5
    syscall
    bgtz $v0, loop
    neg $v0, $v0

loop:
    add $a0, $a0, $t0
    addi $t0, 2
    addi $t1, 1
    blt $t1, $v0, loop

end:
    li $v0, 1
    syscall
    la $a0, n
    li $v0, 4
    syscall
    li $v0, 10
    syscall

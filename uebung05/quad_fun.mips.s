.data
n:  .asciiz "\n"

.text
main:
    li $v0, 5
    syscall
    bgtz $v0, skip
    neg $v0, $v0

skip:
    sll $v0, $v0, 1
    la $t0, loop
    li $a0, 0 # Summe

loop:
    addi $a0, $a0, 1
    lh $t1, ($t0)
    addi $t1, $t1, 2
    sh $t1, ($t0)
    ble $t1, $v0, loop

end:
    li $v0, 1
    syscall
    la $a0, n
    li $v0, 4
    syscall
    li $v0, 10
    syscall

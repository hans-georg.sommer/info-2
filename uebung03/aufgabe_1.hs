type Byte = (Nibble, Nibble)

-------------------
-- helper functions
-------------------

nullByte :: Byte
nullByte = ((False, False, False, False), (False, False, False, False))

byte2Bl :: Byte -> [Bool]
byte2Bl ((a, b, c, d), (e, f, g, h)) = [a, b, c, d, e, f, g, h]

bl2Byte :: [Bool] -> Byte
bl2Byte [a, b, c, d, e, f, g, h] = ((a, b, c, d), (e, f, g, h))

negBl :: [Bool] -> [Bool]
negBl [] = []
negBl x  = not (head x) : negBl (tail x)

_int2BoolList :: Int -> Int -> [Bool]
_int2BoolList i 0
    | i > 0 = [True]
    | otherwise = [False]
_int2BoolList i n
    | i >= 2 ^ n = True : _int2BoolList (i - 2 ^ n) (n - 1)
    | otherwise  = False : _int2BoolList i (n - 1)

-- n has to be the maximum power of 2, -> digits - 1
_int2BoolListSigned :: Int -> Int -> [Bool]
_int2BoolListSigned i n
    | i >= 2 ^ n   = _int2BoolListSigned (2 ^ n - 1) n -- clamp i to ...
    | i < -(2 ^ n) = _int2BoolListSigned (-(2 ^ n)) n -- allowed value range
    | i < 0        = True : _int2BoolList (2 ^ n + i) (n - 1)
    | otherwise    = False : _int2BoolList i (n - 1)

int2Nibble :: Int -> Nibble
int2Nibble i = (a, b, c, d)
    where [a, b, c, d] = _int2BoolListSigned i 3

int2Byte :: Int -> Byte
int2Byte i = bl2Byte (_int2BoolListSigned i 7)

showByte :: Byte -> String
showByte x = bool2binString n ++ " " ++ show (bool2dec n) ++ " " ++ show (bool2decTwoComp n)
    where n = byte2Bl(x)

-----------
-- Aufgaben
-----------

-- Aufgabe 1
shiftByte :: Byte -> Byte
shiftByte ((a, b, c, d), (e, f, g, h)) = ((b, c, d, e), (f, g, h, False))

-- Aufgabe 2
signExtension :: Nibble -> Byte
signExtension (a, b, c, d) = ((a, a, a, a), (a, b, c, d))

-- Aufgabe 3
multBitByte :: Bool -> Byte -> Byte
multBitByte False x = nullByte
multBitByte True x = x

-- Aufgabe 4
-- TODO: handling of lists with different lengths
-- TODO: handlings of empty lists
-- for the moment this is not important because only called indirectly
_rippleCarryAdder :: [Bool] -> [Bool] -> Bool -> [Bool]
_rippleCarryAdder a b c
    | length a == 1  = [s]
    | otherwise      = _rippleCarryAdder (init a) (init b) c_out ++ [s]
    where (s, c_out) = fulladder (last a) (last b) c

rippleCarryAdder :: Byte -> Byte -> Byte
rippleCarryAdder x y = bl2Byte (_rippleCarryAdder a b False)
    where a = byte2Bl x
          b = byte2Bl y

-- Aufgabe 5
carrySaveAdder :: Byte -> Byte -> Byte -> (Byte, Byte)
carrySaveAdder x y z = (a, b)
    where a = ((s1, s2, s3, s4), (s5, s6, s7, s8))
          b = ((c1, c2, c3, c4), (c5, c6, c7, c8))
          (s1, c1) = fulladder x1 y1 z1
          (s2, c2) = fulladder x2 y2 z2
          (s3, c3) = fulladder x3 y3 z3
          (s4, c4) = fulladder x4 y4 z4
          (s5, c5) = fulladder x5 y5 z5
          (s6, c6) = fulladder x6 y6 z6
          (s7, c7) = fulladder x7 y7 z7
          (s8, c8) = fulladder x8 y8 z8
          [x1, x2, x3, x4, x5, x6, x7, x8] = byte2Bl x
          [y1, y2, y3, y4, y5, y6, y7, y8] = byte2Bl y
          [z1, z2, z3, z4, z5, z6, z7, z8] = byte2Bl z

-- Aufgabe 6
-- parentheses galore, at least not https://xkcd.com/859/
negByte :: Byte -> Byte
negByte x = rippleCarryAdder (bl2Byte (negBl (byte2Bl x))) (int2Byte 1)

-- Aufgabe 7
_partialProductList :: [Bool] -> Byte -> [Byte]
_partialProductList a b
    | a == []   = []
    | otherwise = multBitByte (last a) b : _partialProductList (init a) (shiftByte b)

multNibble :: Nibble -> Nibble -> Byte
multNibble x y = result
    where result = rippleCarryAdder s6 (shiftByte c6)
          (s6, c6) = carrySaveAdder s5 (shiftByte c4) (shiftByte c5)
          (s5, c5) = carrySaveAdder s3 s4 (shiftByte c3)
          (s4, c4) = carrySaveAdder s2 (shiftByte c1) (shiftByte c2)
          (s3, c3) = carrySaveAdder p7 p8 s1
          (s2, c2) = carrySaveAdder p4 p5 p6
          (s1, c1) = carrySaveAdder p1 p2 p3
          [p1, p2, p3, p4, p5, p6, p7, p8] = _partialProductList (byte2Bl (signExtension x)) (signExtension y)

-- Aufgabe 8
tableMult :: (Nibble -> Nibble -> Byte) -> [(Nibble, Nibble)] -> String
tableMult f a
    | a == [] = ""
    | otherwise = showNibble x ++ " * " ++ showNibble y ++ " = " ++ showByte (f x y) ++ "\n" ++ tableMult f (tail a)
    where x = fst (head a)
          y = snd (head a)

----------------
-- testfunctions
----------------

-- execute: putStrLn test_multNibbleTable
test_multNibbleTable :: String
test_multNibbleTable = tableMult multNibble [(int2Nibble a, int2Nibble b) | a <- [(-8) .. 7], b <- [(-8) .. 7]]

test_rippleCarryAdder :: Int -> Int -> String
test_rippleCarryAdder a b = showByte (rippleCarryAdder (int2Byte a) (int2Byte b))

-- does not prevent or detect overflows
test_carrySaveAdder :: Int -> Int -> Int -> String
test_carrySaveAdder x y z = showByte (rippleCarryAdder a (shiftByte b))
    where (a, b) = carrySaveAdder (int2Byte x) (int2Byte y) (int2Byte z)








----------------------------
-- übernommen aus Übung 2 --
----------------------------

type Nibble = (Bool, Bool, Bool, Bool)

-- helper functions

-- convert a list of Boolean values to a binary string
bool2binString :: [Bool] -> String
bool2binString [] = ""
bool2binString bl
    | head bl = '1' : bool2binString (tail bl)
    | otherwise = '0' : bool2binString (tail bl)

-- convert a list of Boolean values to a decimal number
bool2dec :: [Bool] -> Int
bool2dec [] = 0
bool2dec bl
    | head bl = 2 ^ length(tail bl) + (bool2dec (tail bl))
    | otherwise = bool2dec (tail bl)

-- convert a list of Boolean values to a decimal number using two's complement
bool2decTwoComp :: [Bool] -> Int
bool2decTwoComp [] = 0
bool2decTwoComp bl
    | head bl = (-1) * (2 ^ length(tail bl)) + (bool2dec (tail bl))
    | otherwise = bool2dec bl

xor :: Bool -> Bool -> Bool
xor a b = (a || b) && (not (a && b))

------------
-- Aufgabe 1
------------

-- show the binary, decimal an decimal/two's complement representations of a nibble
showNibble :: Nibble -> String
showNibble x = bool2binString n ++ " " ++ show (bool2dec n) ++ " " ++ show (bool2decTwoComp n)
    where n = [a, b, c, d]
          (a, b, c, d) = x

------------
-- Aufgabe 2
------------
-- CHANGED: switch output order
fulladder :: Bool -> Bool -> Bool -> (Bool, Bool)
fulladder a b c_in = (s, c_out)
    where c_out = (a && b) || (t && c_in)
          s = xor t c_in
          t = xor a b
